******* This project is discontinued in its Python form

******* Soon it will be migrated to Haskell or some other language if I find it 

******* more suitable. There will be no more commits, except some to fill up holes

******* in the preprocessor part. 

******* For now I don't plan on implementing the runtimer part in Python

******* - maybe some day, if the need arises...

Upp - Universal Preprocessor and Runtimer

This program does C-like preprocessing on arbitrary files (the preprocessor part)
and emits to subprocess - potentially code interpreters - preprocessed sections, 
so they may be evaluated and their result used later in the same file (the runtimer part).

Currently the runtimer is not implemented and the following sections speaking of it
are only theoretical, they illustrate how I conceive it and how eventually it will
be implemented.

Preprocessor:
    Preprocessor directive is a line begining with PREPROC_PREFIX* and having as a
    second token a reserved word. PREPROC_PREFIX is conceived as being a combination
    of a comment char and another, annotating symbol indicating to the preprocessor
    the special status of the line. This is not a requirement, but it is useful, if 
    a script should be used directly by feeding it directly to its respective interpreter
    and indirectly by passing it to the interpreter: the comment Char assures that 
    preprocessor directives don't break existing scripts.
    
    Directives:
    #@def SYMBOL <MEANING>
        Defines a symbol SYMBOL with meaning MEANING which is replaced wherever found
        with its MEANING. If meaning is not specified, the symbol will be simply defined
        as empty, so it may be used later with conditional directives, which do not currently
        compare meanings.
        
    #@undef SYMBOL
        Undefines symbol and any further occurances of that symbol will not be replaced by its meaning.
        Conditionnal directives asking whether the symbol is defined will evaluate to false.
    #@if <def SYMBOL|undef SYMBOL|expr>
        Conditional direcitve - checks whether a symbol is defined. Currently
        longer logical expression are not implemented. The syntax is #@if +
        logical operator (def for defined | undef for undefined) + SYMBOL.
        If the conditional evaluates to false, the section starting with it and
        terminating by the corresponding #@end if is ignored, that is not emited
        to the resulting file.
        
    #@block < <copy|paste>|<do_magic|as_is> > <emit|no_emit> NAME
        Declares a block containing all lines between starting block directive
        and corresponding #@end block NAME directive. Syntax is a bit complicated
        at first glance but its really logical it decomposes to:
        #@block ACTION OPTIONS NAME_OF_BLOCK
        ACTIONs are:
            copy - copies to the definition of the block lines following the directive
            paste - puts at the place of the directive all lines in the directive buffer
            reinject - do as if the lines in block are encountered for the first time.
                    this maybe usefull if the block is created with as_is option
                    that way directives in the block will be reinterpreted at an arbitrary
                    place in the code possibly multiple times if the programmer desires it.
            reinterprete - lines in the block buffer will be reinterpreted with respect
                        to symbols defined at the moment of evaluation. The resulting
                        reinterpretation shall not be emitted:
        Options are:
            do_magic        - apply preprocessing to lines in block
            as_is           - do not preprocess lines, copy them as they are to the buffer
            do_emit         - do emit lines as they are encountered to the resulting file
            no_emit         - do not emit lines to the resulting file. They will be copied
                                            to the buffer and used when demanded
        The NAME of the block is mandatory as closing and references to blocks are made
        via their NAMEs.
    #@assert <equal|not_equal> SYMBOL1 SYMBOL2
        Evaluates whether the meaning of SYMBOL1 and SYMBOL2 is (not) equal. If 
        the comparison yields false, execution is stopped. Currently only defines
        can be compared to other defines, and blocks can be compared to blocks.
        Block to define cannot be compared at least for now.
    #@end <if|block NAME>
        Terminates an if section or a block definition whose name is NAME.
        #@end has different behaviour with ifs and blocks:
        
        #@if def THING              // First if
        #@ if def ANOTHER_THING     // SECOND IF
        #@ end if                   // ENDS SECOND IF
        #@end if                    // ENDS FIRST IF
        
        For flexibility, #@end closes the block specified with by the name parameter:
        
        #@block copy as_is no_emit FIRST_BLOCK
        #@block copy as_is do_emit SECOND_BLOCK
        // Code...
        // Code...
        // Code..
        #@end block FIRST_BLOCK                     // have the end of a block called in FIRST_BLOCK in SECOND_BLOCK
        // more code
        #@end block SECOND_BLOCK                    // terminate second block
        
        // Afterwards if a SECOND_BLOCK is reinjected it will evaluate the #@end block directive
        // Or if FIRST_BLOCK is reinjected it will start a another block called SECOND_BLOCK
        // the closing of which must be taken care of the programmer as it is not in FIRST_BLOCK
        
        This approach to ending blocks exposes programmers to errors, which can be evaded
        by closing blocks in the order they were opened. Enforcing it will make the preprocessor
        lose some of its power.

Runtimer:
    The runtimer is a superset of the preprocessor with auxiliary directives for creating process
    emitting to those process and gathering their return values.
    The main concepts here are those of slaves and a master. The slaves are the workers who
    accept input from the master (in our case the preprocessor) and interprete it, assuming
    it is code.
    Due to limitations in operating systems and for the sake of portability the communication
    between the master and the slaves should be implemented with named pipes - two for each slave:
        - one input pipe which it reads and to which the master writes the data
        - one output pipe which the slave writes to and which the master reads
    This implementation is preferred because all major OSes support named pipes (even Windows).
    
    Runtimer directives:
    #@slave <spawn|kill> NAME CMD
        The #@slave directive 
            -creates a new subprocess with a name NAME, executing CMD, which 
            normally should be a code interpreter like /bin/bash or /usr/bin/python
            when invoked with the option *spawn*
            - kills the slaving NAME when invoked with the option *kill*
        NB: kill doesn't need CMD
    
    #@slave <take|bring> NAME SYMBOL
        - 'take' feeds the meaning of SYMBOL (which may be a block) to the slave
            this is not blocking, and the master resumes its activity after the
            feeding is over
        - 'bring' takes output from slave and puts it in SYMBOL (an as_is block 
            which can be later reinjected) this is a blocking call, and the master
            cannot continue before the slave brings him what he desires.
            A timeout assures that a slave is not in an infinite loop.
            
----------------------------------------------------------------------------------

*Currently the preprocessor is in a working draft, and no command line options 
are implemented. However all of the directive names and options can be changed
manually in the code.