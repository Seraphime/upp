
#@ifndef INCL.SH

#@ifndef ECHO_OPTIONS
#@def ECHO_OPTIONS -n
#@end if
#@def EXTERNAL_DEFINE
#@incl functions.sh

function say_hello {
  echo ECHO_OPTIONS "Hello, "
  if [ "$1" != "" ]; then
    echo "$1!"
  else
    echo "World!"
  fi
}

#@ifdef STUPID_FUNCTION
function do_stuff {
    echo "Can i haz roots ? [You can say no ]"
    su
}
#@end if

#@end if
