#@def EMPTY
#@def ANOTHER_EMPTY

#@assert equal EMPTY ANOTHER_EMPTY
#@assert undef ONE
#@assert undef TWO
#@def ONE 1
#@def ONE_ONE 1
#@def TWO 2

#@assert equal ONE ONE_ONE
#@assert not_equal ONE TWO

#@block copy as_is no_emit FIRST

echo a single line

#@end block FIRST

#@block copy as_is no_emit SECOND

echo a single line

#@end block SECOND

#@assert equal FIRST SECOND

ALL PASSED
