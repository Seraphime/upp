#@def PLUS(a, b) a + b
#@def MINUS(a,b) a-b
#@def PLUS_AND_MINUS(a,b,c) a + b -c
#@def FOUR_ARGS(a, b, c, d)
#@def FIVE_ARGS(a, b, c, d,e)
#@def SIX_ARGS(a, b, c, d,e,f)
#@def SEVEN_ARGS(a, b, c, d,e,f,g)
#@def EIGHT_ARGS(a, b, c, d,e,f, g,    h)
#@def NINE_ARGS(a, b, c, d,e,f, g,     h, j)
#@def TEN_ARGS(a, b, c, d,e,f,g,h,j,k)
#@def TWO_PARENTESES(a,b) (a, b)
#@def LONGER_ARG_NAMES(first_arg, second_arg, third_after_second_and_first_arg)


PLUS(2, 3) 
MINUS(10,7)
PLUS_AND_MINUS(2, 7, 4)