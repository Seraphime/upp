#############################################################################
# This test file is inspired by a file I found in the Clang project			#
# source code. No copyright infringement intended. Any further use			#
# of this file or some derivative of it must comply to the Clang project	#
# license agreement. Please refer to Clang's home page. The original file	#
# can be found on github (and I suppose) any other git mirror of Clang		#
#############################################################################

#@def A1(x, y) x  y
#@def A2(x, y) A1(x, y), A1(x, y)
#@def A3(x, y) A2(x, y), A2(x, y)
#@def A4(x, y) A3(x, y), A3(x, y)
#@def A5(x, y) A4(x, y), A4(x, y)
#@def A6(x, y) A5(x, y), A5(x, y)
#@def A7(x, y) A6(x, y), A6(x, y)
#@def A8(x, y) A7(x, y), A7(x, y)
#@def A9(x, y) A8(x, y), A8(x, y)
#@def A10(x, y) A9(x, y), A9(x, y)
#@def A11(x, y) A10(x, y), A10(x, y)

A11(This_is, spartaaaa)
