#!/bin/bash

#@def MAGIC 2
#@def VOID
#@def whatever "nanana"
#@def COMMAND pgrep
#@def PRESENTATION "My name is"
#@def NAME " Hello World"
#@def SAY echo
#@def YOUR
#@def STUPID_FUNCTION
#@incl incl.sh
#@incl functions.sh
#@incl incl.sh

#@ifdef VOID
    echo "We have VOID"
#@def OPTIONS -ne
#@end if

#@ifndef NOT_DEFINED

#@def MULTI_LINE     echo "First line";
#>  echo "Second line";
#>  echo "Third line"

#@ifdef undefined
#@end if

#@end if

#@ifdef EXTERNAL_DEFINE
  echo "We got the external define"
#@end if

COMMAND python
MULTI_LINE

SAY OPTIONS  PRESENTATION NAME
