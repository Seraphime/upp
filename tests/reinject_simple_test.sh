#@block copy no_emit as_is TEST_SOURCE
#@block copy no_emit as_is TEST_BLOCK

WRITE this is a simple line

#@end block TEST_BLOCK
#@end block TEST_SOURCE
echo "-----------------------SOURCE-------------------------------"
#@block paste TEST_SOURCE
echo "-----------------------SOURCE-------------------------------"

echo "after #@def WRITE printf"
#@def WRITE printf
#@block reinject TEST_BLOCK
echo "end after #@def WRITE printf"


echo "after #@undef WRITE printf && #@def line \"quoted line\""
#@undef WRITE
#@def line "quoted line"
#@block reinject TEST_BLOCK
echo "end after #@undef WRITE printf && #@def line \"quoted line\""