#@block copy do_magic no_emit BLOCK_ONE
#@block copy as_is no_emit BLOCK_TWO
#@def MAGIC blocks
#@def WRITE echo
WRITE "This is the first test for " MAGIC
WRITE "Copies a block and pastes it multiple times"
#@undef WRITE
#@def WRITE printf
WRITE "Blocks are like multilines macros but have more options"
WRITE "I hope you like them as much as macros"
#@end block BLOCK_TWO
#@end block BLOCK_ONE

echo "-----------------First-Time---------------------------"
echo "                                                      "
#@block paste BLOCK_ONE
echo "                                                      "
echo "-----------------Second-Time--------------------------"
echo "                                                      "
#@block paste BLOCK_TWO
echo "                                                      "
echo "-----------------Third-Time---------------------------"
echo "                                                      "
#@block paste BLOCK_ONE
echo "-----------------ORIGINAL-----------------------------"
#@block paste BLOCK_TWO
echo "                                                      "
echo "-----------------End-Test-----------------------------"
