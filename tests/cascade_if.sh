Test file: checks whether upp manages right ifs and endifs.
The two tests must succeed:
###########################################################
#@ifndef SUCCESS_NOT_DEF
#@ ifndef SUCCESS_NOT_DEF
#@  ifndef SUCCESS_NOT_DEF
#@   ifndef SUCCESS_NOT_DEF
#@    ifndef SUCCESS_NOT_DEF
#@     ifndef SUCCESS_NOT_DEF
#@      ifndef SUCCESS_NOT_DEF
#@       ifndef SUCCESS_NOT_DEF

#@       def SUCCESS_NOT_DEF

#@       end if
#@      end if
#@     end if
#@    end if
#@   end if
#@  end if
#@ end if
#@end if

#@ifndef SUCCESS_NOT_DEF
ifndef cascade failed 
#@end if

#@ifdef SUCCESS_NOT_DEF
ifndef cascade success
#@ ifdef SUCCESS_NOT_DEF
#@  ifdef SUCCESS_NOT_DEF
#@   ifdef SUCCESS_NOT_DEF
#@    ifdef SUCCESS_NOT_DEF
#@     ifdef SUCCESS_NOT_DEF
#@      ifdef SUCCESS_NOT_DEF
#@       ifdef SUCCESS_NOT_DEF

#@       def SUCCESS_IFDEF

#@       end if
#@      end if
#@     end if
#@    end if
#@   end if
#@  end if
#@ end if
#@end if

#@ifdef SUCCESS_IFDEF
ifdef cascade success
#@end if
#@ifndef SUCCESS_IFDEF
ifdef cascade failed
#@end if