#!/usr/bin/env python

"""
    Wanna-be universal C like preprocessor.
    No warranties given. This piece of software
    is released under MITPL. 
    @author: Seraphime Kirkovski
    @email: kirkseraph@gmail.com
"""

from sys import argv, exit, stderr, stdout
import re

#@def PPREFIX "#@"
#@def PMULTI "#>"
#@def PDEF "def"
#@def PUDEF "undef"
#@def PIFDEF "ifdef"
#@def PIFNDEF "ifndef"
#@def PENDIF "endif"
#@def PINCL "incl"

PREPROC_PREFIX = PPREFIX
PREPROC_MULTI = PMULTI
DEFINE = PDEF
UNDEFINE = PUDEF
IFDEF = PIFDEF
IFNDEF = PIFNDEF
ENDIF = PENDIF
INCL = PINCL
COMMENT_CHAR = "#"

# The following are useless and can be safely removed
# For the moment they are staying in case they are needed
# for some sort of help message
DEFINE_DIRECITVE = PREPROC_PREFIX + DEFINE
UNDEFINE_DIRECTIVE = PREPROC_PREFIX + UNDEFINE
IFDEF_DIRECTIVE = PREPROC_PREFIX + IFDEF
IFNDEF_DIRECTIVE = PREPROC_PREFIX + IFNDEF
ENDIF_DIRECTIVE = PREPROC_PREFIX + ENDIF
INCL_DIRECTIVE = PREPROC_PREFIX + INCL
# End of useless constants

# Regex patterns
DEFINE_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + DEFINE + "\s")
UNDEF_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + UNDEFINE + "\s")
IFDEF_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + IFDEF + "\s")
IFNDEF_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + IFNDEF + "\s")
ENDIF_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + ENDIF + "(\s)*")
INCL_PATTERN = re.compile(PREPROC_PREFIX + "(\s)*" + INCL + "\s")

class Replacer:
    def __init__(self):
        self.__hash = -1
        self.pattern = None
        self.rep = None

    def plain_replace(self, line, state):
        if len(state.symbols) == 0:
            return line
      #  self.__prepare(symbols)
        return self.__do_replace(line, state)

    def replace_in_directive(self, line, state, directive):
        if len(state.symbols) == 0:
           return line
       # self.__prepare(symbols)
        line = line.rstrip()
        start_d = line.find(directive[0])
        if len(line) - start_d == len(directive):
           return line
        result = self.__do_replace(line[start_d + len(directive):], state)
        return line[:start_d + len(directive)] + result

    def __compile_regex(self,symbols):
        rep = dict()
        for key, value in symbols.iteritems():
            if value != None:
                rep[key] = value
        rep = dict((re.escape(k), v) for k, v in rep.iteritems())
        self.rep, self.pattern = rep, re.compile("|".join(rep.keys())) 

    def __prepare(self, symbols):
        if self.__hash != hash(frozenset(symbols)) or not self.pattern:
          self.__compile_regex(symbols)
          self.__hash =  hash(frozenset(symbols))

    def __do_replace(self, line, state):
        res = " "
        i = 0
        while i < len(line):
            st, j = Helper.get_symbol(line, i)
            if not st in state.symbols:
               res += st
               if j == i:
                   while j < len(line) and (not line[j].isalpha() and line[j] != '_'):
                       res += line[j]
                       j += 1
               i = j
            else:
                dir = state.symbols[st]
                if dir.is_invoked:
                    args = list()
                    while j < len(line) and line[j].isspace():
                        j += 1
                    if  j < len(line) and line[j] == '(':
                        j += 1
                        while j < len(line) and line[j] != ')':
                            arg, j = Helper.get_argument(line, j)
                            if j >= len(line):
                                stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.\n")
                                return              # Should (?) quit  
                            while j < len(line) and line[j].isalpha():
                                arg += line[j];
                                j += 1
                            if  j > len(line) or (line[j] != ')' and line[j] != ',' and line[j].isspace == False):
                                stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.\n")
                                return              # Should (?) quit 
                            args.append(arg)
                            if line[j] == ')':
                                break 
                            j += 1
                        if len(args) != dir.needs():
                            stderr.write("Error: Macro called %d arguments when it needs %d\n" % (len(args), dir.needs()))
                            return
                        res += dir.apply_args(args)
                    else:
                        stderr.write("Debug: invokable macro symbol encountered with no parenthesis\n")
                        res += line[i:j - 1]
                    j += 1
                    i = j
                else:
                    res += dir.get_meaning()
        return res
            
class State:
    """
        Internal class. This class is used only for internal purposes.
        It is supposed to pass state around the functions.
        That is:    the lines or (some day) the file handler
                    the index of the current line
    """
    def __init__(self, lines, index=0, symbols=None):
        if type(lines) != list:
           if type(lines) != file:
                raise TypeError("State object is created with a list of lines or file object")
           lines = lines.readlines()
        if symbols == None:
           symbols = dict()
        self.symbols = symbols
        self.fstack = []
        self.lines = lines
        self.index = index
        self.endifs_awaited = 0

    def skip_conditional(self):
        endifs_awaited = 0
        while self.index < len(self.lines):
            if re.search(IFDEF_PATTERN, self.lines[self.index]) != None:
                endifs_awaited += 1
            elif re.search(IFNDEF_PATTERN, self.lines[self.index]) != None:
                endifs_awaited += 1
            elif re.search(ENDIF_PATTERN, self.lines[self.index]) != None:
                if endifs_awaited <= 0:
                    stderr.write("Error: unmatched conditional and endifs\n")
                    exit(0)
                endifs_awaited -= 1
            if endifs_awaited == 0:
                break
            self.index += 1

    def decrement_endifs(self):
        if self.endifs_awaited <= 0:
            print "Unbalanced conditional and end conditional statements" #raise Exception("Unbalanced conditional and end conditional statements")
        self.endifs_awaited -= 1

    def load_file(self, f):
        self.fstack.append((self.lines, self.index))
        self.lines = f.readlines()
        self.index = 0
        f.close()

    def unload_file(self):
        if len(self.fstack) > 0:
            self.lines, self.index = self.fstack.pop()
        else:
            self.lines, self.index = None, -1

    def load_nextline(self):
        if self.index + 1 < len(self.lines):
           self.index += 1
        else:
            self.unload_file()

    def can_iter(self):
       return not (self.index < 0 or not self.lines)

class Directive:
    """ 
        Internal class. This class is used only for internal purposes.
        It must provide a straightforward interface to apply all directives.
        It is constructed from functions create_$(DIRECTIVE_NAME) which
        use the setters to dynamically add when needed properties.
        The existence of only one properties must be guaranteed:
        self._symbol - it is the symbol to be defined, included or ifdefed:
            #@(un)def SYMBOL meaning
            #@incl SYMBOL                       - must be a filename
            #@ifdef SYMBOL                      - symbol defined by @def, may not have meaning
        Other properties depend on the implementation of the directive
        and must only be referenced from the static methods given bellow

        Only a getter for the symbol property must be exposed although it will only serve
        for the define directives
    """
    def eval(self, state): 
        pass

    def __init__(self, evaluator):
        """
            @param evaluator: function to be assigned on self.eval
            Normally it will be one of the static methods bellow
        """
        self.eval = evaluator
        self.is_invoked = False

    def set_meaning(self, meaning):
        self._meaning = meaning
    
    def set_symbol(self, symbol):
        self._symbol = symbol
    
    def set_arguments(self, args):
        self.args = args;

    def get_symbol(self):
        return self._symbol

    def get_meaning(self):
        return self._meaning

    def needs(self):
        return self._arg_count

    def apply_args(self, args):
      i = 0
      formatter = list(self.args)
      while i < len(self.args):
        formatter[i] = args[self.args[i]]
        i += 1
      return (self._meaning % tuple(formatter))

    @staticmethod
    def _ifdef(self, state):
        if self.get_symbol() in state.symbols:
            state.endifs_awaited += 1
            return
        state.skip_conditional()

    @staticmethod
    def _ifndef(self, state):
        if not self.get_symbol() in state.symbols:
            state.endifs_awaited += 1
            return
        state.skip_conditional()

    @staticmethod
    def _endif(self, state):
        state.decrement_endifs()

    @staticmethod
    def  _create_inclguard(line):
        return "_" + filename.replace(".", "_").upper() + "_"
        
    @staticmethod
    def _incl(self, state):
        fh = None
        guard = Directive._create_inclguard(self.get_symbol())
        if guard in state.symbols:
            return
        state.symbols[guard] = None
        try:
            fh = open(self.get_symbol(), "r")
        except:
            raise Exception("File cannot be opened for reading")
        state.load_file(fh)

    @staticmethod
    def _define(self, state):
        state.symbols[self.get_symbol()] = self
        if not self.is_invoked:
            return
        hashes = list()  
        for arg in self.args:
            _hash = hash(arg)
            hashes.append(_hash)
        self.set_meaning(self.get_meaning().strip())
        args = list()
        i = 0
        meaning = self.get_meaning()
        while i < len(meaning):
            while i < len(meaning) and meaning[i].isspace(): i += 1
            arg, start = Helper.get_symbol(meaning, i)
            _hash = hash(arg)
            if start == len(meaning):
                start -= 1
            j = 0
            while j < len(hashes) and _hash != hashes[j]:
                 j += 1
            if j < len(hashes):
                 meaning = meaning[:i] + "%s" +  (meaning[start:] if start != i else "")  
                 if len(arg) < 2:
                    start += 2
                 args.append(j)
            i = start
        args = args if len(args) != 0 else None
        self._arg_count = len(self.args)
        self.args = args
        self.set_meaning(meaning)
                
class Helper:
    @staticmethod
    def is_symbolchar(c):
        return c.isalpha() or c.isdigit() or c == '_'
    @staticmethod 
    def get_symbol(line, i=0):
        symbol = ""
        while i < len(line) and line[i].isspace():
            i += 1
        if i >= len(line) or (line[i].isalpha() == False and line[i] != '_'):
            # stderr.write("Invalid token sequence in define : %s" % line)
            return "", i
        while i < len(line) and (Helper.is_symbolchar(line[i])):
            symbol += line[i]
            i += 1
        return symbol, i
    @staticmethod
    def get_argument(line, i=0):
        while i < len(line) and line[i].isspace():
            i += 1
        arg = ""
        escape = False
        while i < len(line) and not (line[i] == ',' or line[i] == ')'):
            arg += line[i]
            if line[i] == '\\':
                escape = True
            elif not escape and line[i] == '"':
               i += 1
               escape = False
               while i < len(line) and (not escape and line[i] != '"'):
                   if line[i] == '\\':
                      escape = True
                   arg += line[i]
                   i += 1
               arg += line[i]
            i += 1
        if i == len(line) or (line[i] != ',' and line[i] != ')'):
            return None, i
        return arg.rstrip(), i

def create_conditional(line, isdef):
    ret = None
    symbol = ""
    i = 0
    while i < len(line) and line[i].isspace():
        i += 1
    while i < len(line) and (line[i].isalpha() or line[i] == '_'):
        symbol += line[i]
        i += 1
    if symbol == "":
        stderr.write("Invalid token sequence in conditional : %s" % line)
        return ret
    if isdef:
       ret = Directive(Directive._ifdef)
    else:
       ret = Directive(Directive._ifndef)
    ret.set_symbol(symbol)
    return ret

def create_endif():
    return Directive(Directive._endif)

def create_incl(line):
    st = ""
    i = 0
    while i < len(line) and line[i].isspace():
        i += 1
    while i < len(line) and not line[i].isspace():
        st += line[i]
        i += 1
    if st == "":    
        stderr.write("Warning: no macro argument given for include: %s" % line)
        return None
    ret = Directive(Directive._incl)
    ret.set_symbol(st)
    return ret

def create_define(line, lines, index, isinvoked):
    ret = None
    symbol, i = Helper.get_symbol(line)
    if symbol == "":
        stderr.write("Invalid token sequence in define : %s" % line)
        return ret
    ret = Directive(Directive._define)
    ret.set_symbol(symbol)
    if i < len(line) and line[i] == '(':
        ret.is_invoked = True
        arguments = list()
        while line[i] != ')':
            i += 1;
            arg = ""
            while i < len(line) and line[i].isspace():
                i += 1
            if i > len(line) or line[i].isalpha() == False:
                stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.")
                return None, index 
            while i < len(line) and line[i].isalpha():
                arg += line[i];
                i += 1
            if line[i] != ')' and line[i] != ',' and line[i].isspace == False:
                stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.")
                return None, index
            arguments.append(arg)
        ret.set_arguments(arguments)
    if line[i] == ')':
        i += 1
    meaning = line[i:].rstrip()
    while index + 1 < len(lines) and lines[index + 1].find(PREPROC_MULTI) == 0:
        index += 1
        meaning += "\n" + lines[index][len(PREPROC_MULTI):]
    ret.set_meaning(meaning)
    return ret, index

def get_directive(lines, index):
    i = 2
    lenline = len(lines[index])
    is_invoked = False
    while i < lenline and lines[index][i].isspace():
        i += 1
    if i >= lenline:
        return None, index
    dir = ""
    while i < lenline and lines[index][i].isalpha():
        dir += lines[index][i]
        i += 1
    if  dir == ENDIF:
        return create_endif(), index
    if lines[index][i] == '(':
        is_invoked = True
    if dir == DEFINE:
        directive, index = create_define(lines[index][i:], lines, index, is_invoked)
    elif is_invoked:
        stderr.write("Warning: Macro ignored. Invalid '(' token in macro at line %d : %s\n" % (index, lines[index]))
        return None, index
    elif dir == INCL:
        if lines[index][i].isspace() == False:
            stderr.write("Warning: Macro ignored. Invalid token in macro at line %d : %s\n" % (index, lines[index]))
            return None, index  
        directive = create_incl(lines[index][i:])
    elif dir == IFDEF:
        directive = create_conditional(lines[index][i:], True)
    elif dir == IFNDEF:
        directive = create_conditional(lines[index][i:], False)
    else: # False alarm ?!
        directive = None
    return directive, index
    

def parse_directives(lines, symbols=None):
    replacer = Replacer()
    last_define = None
    preprocessed_lines = list()
    state = State(lines, 0, symbols)
    while state.can_iter():
        if len(state.lines[state.index]) >= 2 and state.lines[state.index][0:2] == PREPROC_PREFIX:
            if re.search(DEFINE_PATTERN, state.lines[state.index]) != None:
                state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, DEFINE)
            elif re.search(UNDEF_PATTERN, state.lines[state.index]) != None:
                state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, UNDEFINE)
            elif re.search(IFDEF_PATTERN, state.lines[state.index]) != None:
               state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, IFDEF)
            elif re.search(IFNDEF_PATTERN, state.lines[state.index]) != None:
                state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, IFNDEF)
            elif re.search(INCL_PATTERN, state.lines[state.index]) != None:
                state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, INCL)
            elif re.search(ENDIF_PATTERN, state.lines[state.index]) != None:
                state.lines[state.index] = replacer.replace_in_directive(state.lines[state.index], state, ENDIF)
            directive, index = get_directive(state.lines, state.index)
            if directive:
                directive.eval(directive, state)
        else:
            if state.lines[state.index] != "\n" and  state.lines[state.index] != "\r\n":
                preprocessed_lines.append(replacer.plain_replace(state.lines[state.index], state))
        state.load_nextline()
    return preprocessed_lines

if __name__ == "__main__":
    fh = open(argv[1])
    pl = parse_directives(fh.readlines())
    fh.close()
    for l in pl:
        stdout.write(l)