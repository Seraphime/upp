#!/usr/bin/env python

"""
    Wanna-be universal C like preprocessor.
    No warranties given. This piece of software
    is released under MITPL.
    @author: Seraphime Kirkovski
    @email: kirkseraph@gmail.com
"""

from sys import argv, stderr, stdout
import re

COMMENT_CHAR = "#"
PREPROC_PREFIX = COMMENT_CHAR + "@"
PREPROC_MULTI = COMMENT_CHAR + ">"
DEFINE = "def"
UNDEFINE = "undef"
IF = "if"
DEF = "def"
NDEF = "ndef"
IFDEF = IF + DEF
IFNDEF = IF + NDEF
BLOCK = "block"
END = "end"
INCL = "incl"
ASSERT = "assert"

END_OPTIONS = {
    "if": IF,
    "block": BLOCK
}

COPY = "copy"
CUT = "cut"
PASTE = "paste"
REINJECT = "reinject"
REINTERPRETE = "reinterprete"

BLOCK_OPTIONS = {
    "copy"   : COPY,
    "paste"  : PASTE,
    "reinject" : REINJECT,
    "reinterprete" : REINTERPRETE 
}

IF_OPTIONS = {
    "def" : DEF,
    "ndef" : NDEF
}

ASSERT_EQUAL     = "equal"
ASSERT_NOT_EQUAL = "not_equal"
ASSERT_DEFINED   = DEF
ASSERT_UNDEFINED = UNDEFINE

ASSERT_OPTIONS = {
"equal"     : ASSERT_EQUAL,
"not_equal" : ASSERT_NOT_EQUAL,
"def"       : ASSERT_DEFINED,
"undef"     : ASSERT_UNDEFINED
}

AS_IS_MODIFIER_INDEX = 0
EMIT_MODIFIER_INDEX = 1

def _have_as_is_modifier(modifs):
    modifs[AS_IS_MODIFIER_INDEX] = False
def _have_do_magic_modifier(modifs):
    modifs[AS_IS_MODIFIER_INDEX] = True
def _have_no_emit_modifier(modifs):
    modifs[EMIT_MODIFIER_INDEX] = False
def _have_do_emit_modifier(modifs):
    modifs[EMIT_MODIFIER_INDEX] = True

def _get_as_is_modifier(modifs):
    return modifs[AS_IS_MODIFIER_INDEX]
def _get_emit_modifier(modifs):
    return modifs[EMIT_MODIFIER_INDEX]

AS_IS = "as_is"
DO_MAGIC = "do_magic"
NO_EMIT = "no_emit"
DO_EMIT = "do_emit"

MODIFIERS = {
    AS_IS      : _have_as_is_modifier,
    DO_MAGIC   : _have_do_magic_modifier,
    NO_EMIT    : _have_no_emit_modifier,
    DO_EMIT    : _have_do_magic_modifier
}

def create_modifier_list():
    assert len(MODIFIERS) % 2 == 0
    modifiers = [False] * (  len(MODIFIERS) / 2 )
    # modifiers[0] = False # evaluate directives and replacements <=> as_is = False <=> do_magic = True
    # modifiers[1] = False # do emit lines in the block in the output <=> no_emit = False
    return modifiers

UPP_DEFAULT = "UPP__DEFAULT__PPU"
EMIT_PROTECTOR = (UPP_DEFAULT, True)

# Regex patterns
DEFINE_PATTERN = re.compile(PREPROC_PREFIX + r"(\s)*" + DEFINE + r"\s")
UNDEF_PATTERN = re.compile(PREPROC_PREFIX + r"(\s)*" + UNDEFINE + r"\s")
ENDIF_PATTERN = re.compile(PREPROC_PREFIX + r"(\s)*" + END + r"(\s)*" + IF + r"(\s)*")
INCL_PATTERN = re.compile(PREPROC_PREFIX + r"(\s)*" + INCL + r"\s")

# TODO: Make one big pattern out of these
CONDITIONAL_PATTERNS = [
    re.compile(PREPROC_PREFIX + r"(\s)*" + IF + r"\s"),
    re.compile(PREPROC_PREFIX + r"(\s)*" + IFDEF + r"\s"),
    re.compile(PREPROC_PREFIX + r"(\s)*" + IFNDEF + r"\s")
]


WARN_PREFIX = "Warning: "
ERROR_PREFIX = "Error: "

class Replacer(object):
    """ Internal Class. Previously it used to have some state, but now
        all of its members can become static.
    """
    @staticmethod
    def plain_replace(line, state):
        """ Does replacement inside of non-directive lines """
        if len(state.symbols) == 0:
            return line
        return Replacer._do_replace(line, state)

    @staticmethod
    def _do_replace(line, state, res=None):
        """ Implements replacements """
        if not res:
            res = list()
        i = 0
        while i < len(line):
            while i < len(line) and line[i].isspace():
                res += line[i]
                i += 1
            st, j = Helper.get_nextword(line, res, i)
            if not st in state.symbols:
               res += st
            else:
                directive = state.symbols[st]
                isdir = isinstance(directive, DefDirective)
                if isdir and directive.is_invoked:
                    args = list()
                    while j < len(line) and line[j].isspace():
                        j += 1
                    if  j < len(line) and line[j] == '(':
                        j += 1
                        while j < len(line) and line[j] != ')':
                            arg, j = Helper.get_argument(line, j)
                            if j >= len(line):
                                state.error("Illegal token macro definition encountered. Undefined behaviour.")
                                return              # Should (?) quit  
                            while j < len(line) and line[j].isalpha():
                                arg += line[j];
                                j += 1
                            if  j > len(line) or (line[j] != ')' and line[j] != ',' and line[j].isspace == False):
                                state.error("Illegal token macro definition encountered. Undefined behaviour.")
                                return              # Should (?) quit 
                            args.append(arg)
                            if line[j] == ')':
                                break 
                            j += 1
                        if len(args) != directive.needs():
                            stderr.write("Error: Macro called %d arguments when it needs %d\n" % (len(args), directive.needs()))
                            return
                        res += directive.apply_args(args)
                    else:
                        res += line[i:j - 1]
                    j += 1
                elif isdir:
                    res += directive.get_meaning()
            i = j
        return "".join(res)

    def __init__(self):
        raise RuntimeError("Replacer is a static class. Should not be instantiated")

class Helper(object):
    """ Internal class. Contains static helper functions. """
    @staticmethod
    def is_symbolchar(c):
        return c.isalpha() or c.isdigit() or c == '_'

    @staticmethod 
    def get_symbol(line, i=0):
        symbol = ""
        while i < len(line) and line[i].isspace():
            i += 1
        if i >= len(line) or (line[i].isalpha() == False and line[i] != '_'):
            # stderr.write("Invalid token sequence in define : %s" % line)
            return "", i
        while i < len(line) and (Helper.is_symbolchar(line[i])):
            symbol += line[i]
            i += 1
        return symbol, i

    @staticmethod
    def get_argument(line, i=0):
        while i < len(line) and line[i].isspace():
            i += 1
        arg = ""
        escape = False
        while i < len(line) and not (line[i] == ',' or line[i] == ')'):
            arg += line[i]
            if line[i] == '\\':
                escape = True
            elif not escape and line[i] == '"':
               i += 1
               escape = False
               while i < len(line) and (not escape and line[i] != '"'):
                   if line[i] == '\\':
                      escape = True
                   arg += line[i]
                   i += 1
               arg += line[i]
            i += 1
        if i == len(line) or (line[i] != ',' and line[i] != ')'):
            return None, i
        return arg.rstrip(), i

    @staticmethod
    def _skip_quote(i, line, append_to, quote):
        escape = False
        while i < len(line):
            append_to += line[i]
            if not escape and line[i] == quote:
                break
            elif line[i] == '\\':
                escape = True
            else:
                escape = False
            i += 1
        return i

    @staticmethod
    def get_nextword(line, append_to, i=0):
        escape = False
        while i < len(line) and not (line[i].isalpha() or line[i] == '_'):
            append_to += line[i]
            if line[i] == "\\":
                if escape == True:
                    escape = False
                else:
                    escape = True
            elif not escape:
                if line[i] == '"':
                    i = Helper._skip_quote(i + 1, line, append_to, '"')
                elif line[i] == "'":
                    i = Helper._skip_quote(i + 1, line, append_to, "'")
                elif line[i] == '`':
                    i = Helper._skip_quote(i + 1, line, append_to, '`')
                escape = False
            i += 1
        symbol = ""
        while i < len(line) and Helper.is_symbolchar(line[i]):
            symbol += line[i]
            i += 1
        return symbol, i

class State(object):
    """
        Internal class. This class is used only for internal purposes.
        It is supposed to pass state around the functions.
        That is:    the lines or (some day) the file handler
                    the index of the current line
    """

    # Constants for inject_mutex
    _lock = 1
    _release = 0

    def __init__(self, lines, index=0, symbols=None):
        if not isinstance(lines, list):
           if not isinstance(lines, file):
                raise TypeError("State object is created with a list of lines or file object")
           lines = lines.readlines()
        if symbols == None:
           symbols = dict()
        self.symbols = symbols
        self.fstack = []
        self.active_blocks = []
        self.lines = lines
        self.parent = None
        self.index = index
        self.endifs_awaited = 0
        self.warnings = None
        self.errors = None
        self.emit_protector = EMIT_PROTECTOR
        self._ready_lines = list()
        self._inject_mutex = State._release

    def set_parent(self, preprocessor):
        self.parent = preprocessor

    @staticmethod
    def _is_locked(mutex):
        return mutex != State._release

    def _acquire_inject(self):
        assert self._inject_mutex >= State._release, "Mutex was released more times than it is acquired" # > 0
        self._inject_mutex += State._lock

    def _release_inject(self):
        assert self._inject_mutex >= State._lock, "Mutex released more times than it is acquired"
        self._inject_mutex -= State._lock

    def skip_conditional(self):
        endifs_awaited = 0
        while self.index < len(self.lines):
            bStatementFound = False
            for pattern in CONDITIONAL_PATTERNS: # Can do better than that 
                if re.search(pattern, self.lines[self.index]) != None:
                    endifs_awaited += 1
                    bStatementFound = True
                    break
            if not bStatementFound and re.search(ENDIF_PATTERN, self.lines[self.index]) != None:
                if endifs_awaited <= 0:
                    stderr.write("Unmatched conditional and endifs\n")
                    exit(2)
                endifs_awaited -= 1
            if endifs_awaited == 0:
                break
            self.index += 1

    def decrement_endifs(self):
        if self.endifs_awaited <= 0:
            self.error("Unbalanced conditional and end conditional statements")
        self.endifs_awaited -= 1

    def increment_endifs(self):
        """ Increment end directives awaited. """
        # TODO: some error checking would be appropriate here
        self.endifs_awaited += 1

    def load_file(self, f):
        if State._is_locked(self._inject_mutex):
            self._acquire_inject()
        self.fstack.append((self.lines, self.index))
        self.lines = f.readlines()
        self.index = 0
        f.close()

    def unload_file(self):
        if len(self.fstack) > 0:
            self.lines, self.index = self.fstack.pop()
            self.load_nextline()
        else:
            self.lines, self.index = None, -1
        if State._is_locked(self._inject_mutex):
            self._release_inject()

    def load_nextline(self):
        if self.index + 1 < len(self.lines):
           self.index += 1
        else:
            self.unload_file()

    def current_line(self):
        """
            Returns current line. Throws if index is invalid.
            Error checking for that is missing intentionnally
        """
        return self.lines[self.index]

    def can_iter(self):
       """ Indicates whether iteration over file or lines is over. """
       return not (self.index < 0 or not self.lines)

    def define_symbol(self, symbol, obj):
        if not symbol in self.symbols:
            self.symbols[symbol] = obj

    def warn(self, warning):
        if not self.warnings:
            self.warnings = list()
        self.warnings.append(WARN_PREFIX + warning)

    def error(self, error):
        if not self.errors:
            self.errors = list()
        self.errors.append(ERROR_PREFIX + error)

    def append_readyline(self, line, replacer, is_normal_line=True):
        ready_line = replacer.plain_replace(line, self)
        for block in self.active_blocks:
            block.append_line(line, ready_line, is_normal_line)
        if self.should_emit() and is_normal_line:
            self._ready_lines.append(ready_line)

    def get_readylines(self):
        """ Returns preprocessed lines. """
        return self._ready_lines;

    def add_activeblock(self, block):
        """ Adds block to active blocks """
        self.active_blocks.append(block)

    def remove_activeblock(self, symbol):
        if not symbol in self.symbols:
           self.error("Cannot end block at line %d, because it is not defined: %s" % 
                            (self.index, self.current_line()))
           return
        if not isinstance(self.symbols[symbol], BlockDirective):
            self.error("Cannot remove active block %s because it is not a block at line %d : %s" %
                                                            (symbol, self.index, self.current_line()))
            return
        try:
            self.active_blocks.remove(self.symbols[symbol])
        except ValueError:
            self.error("Cannot end block at line %d, because it is not active: %s" % 
                            (self.index, self.current_line()))
        else:
            self.decrement_endifs()
            for block in self.active_blocks:
                if block.should_emit():
                    continue
                self.emit_protector = (block.get_symbol(), False)
            # If this block blocked emission
            # release the emit_protector
            if self.emit_protector[0] == symbol:
               self.emit_protector = EMIT_PROTECTOR
    
    def append_blocklines(self, symbol):
        if symbol not in self.symbols:
            self.warn("Undefined block cannot be appended to preprocessed lines at line %d : %s" %
                                    (self.index, self.current_line()))
            return
        elif not self.symbols[symbol] or not isinstance(self.symbols[symbol], BlockDirective):
            self.error("Requested symbol is not a block at line %d: %s" % (self.index, self.current_line()))
            return
        for line in self.symbols[symbol].block_lines:
            self._ready_lines.append(line)

    def reinject_blockLines(self, symbol):
        if not self.symbols[symbol] or not isinstance(self.symbols[symbol], BlockDirective):
            self.error("Requested symbol is not a block at line %d: %s" % (self.index, self.current_line()))
            return
        if State._is_locked(self._inject_mutex):
            self.warn("Cannot reinject more than one block at a time at line %d : %s" % (self.index, self.current_line()))
            return
        self.fstack.append((self.lines, self.index))
        self.lines = self.symbols[symbol].get_lines()
        self.index = -1
        self._acquire_inject()

    def should_emit(self):
        """ Indicates whether current line should be emitted to result. """
        return self.emit_protector[1]

    def do_emit(self, block):
        if self.emit_protector[1] == True and block.should_emit() == False:
            self.emit_protector = (block.get_symbol(), False)

class Directive(object):
    """ 
        Internal class. This class is used only for internal purposes.
        It must provide a straightforward interface to apply all directives.
        It is constructed from functions create_$(DIRECTIVE_NAME) which
        use the setters to dynamically add when needed properties.
        The existence of only one properties must be guaranteed:
        self._symbol - it is the symbol to be defined, included or ifdefed:
            #@(un)def SYMBOL meaning
            #@incl SYMBOL                       - must be a filename
            #@ifdef SYMBOL                      - symbol defined by @def, may not have meaning
        Other properties depend on the implementation of the directive
        and must only be referenced from the static methods given bellow

        Only a getter for the symbol property must be exposed although it will only serve
        for the define directives
    """
    def eval(self, state):
        """
            _Abstract_ method.
            Overriden in all derived classes.
        """
        raise NotImplementedError("Abstract method should. Not be invoked directly")

    def __init__(self):
        """
             Normally it will be one of the static methods bellow
        """
        self.is_invoked = False
        self._meaning = None

    def set_meaning(self, meaning):
        self._meaning = meaning
    
    def set_symbol(self, symbol):
        self._symbol = symbol

    def get_symbol(self):
        return self._symbol

    def get_meaning(self):
        return self._meaning

class IComparableDirective(Directive):
    """
        Internal abstract class.
        Used to facilitate comparison different directives.
    """
    def __init__(self):
        super(IComparableDirective, self).__init__()

    def equal(self, other):
        raise TypeError("Function on an 'abstract' class called")

class DefDirective(IComparableDirective):
    """ Internal class. Implements Define Directive. """
    def eval(self, state):
        state.symbols[self.get_symbol()] = self
        if not self.is_invoked:
            return
        hashes = list()  
        for arg in self.args:
            _hash = hash(arg)
            hashes.append(_hash)
        self.set_meaning(self.get_meaning().strip())
        args = list()
        i = 0
        meaning = self.get_meaning()
        while i < len(meaning):
            while i < len(meaning) and meaning[i].isspace(): i += 1
            arg, start = Helper.get_symbol(meaning, i)
            _hash = hash(arg)
            if start == len(meaning):
                start -= 1
            j = 0
            while j < len(hashes) and _hash != hashes[j]:
                 j += 1
            if j < len(hashes):
                 meaning = meaning[:i] + "%s" +  (meaning[start:] if start != i else "")  
                 if len(arg) < 2:
                    start += 2
                 args.append(j)
            i = start
        args = args if len(args) != 0 else None
        self._arg_count = len(self.args)
        self.args = args
        self.set_meaning(meaning)

    def needs(self):
        return self._arg_count

    def apply_args(self, args):
      i = 0
      formatter = list(self.args)
      while i < len(self.args):
        formatter[i] = args[self.args[i]]
        i += 1
      return (self._meaning % tuple(formatter))

    def equal(self, other):
        if not isinstance(other, IComparableDirective):
            return False # assert False, "Should not happen"
        if isinstance(other, BlockDirective):
            return False
        else:
            other_lines = other.get_meaning()
            return other_lines == self.get_meaning()

class UndefDirective(Directive):
      def __init__(self):
        super(UndefDirective, self).__init__()

      def eval(self, state):
        if self.get_symbol() not in state.symbols:
           state.warning("Symbol to be undefined is not defined at line %d : %s" %
                                            (state.index, state.current_line()))
           return
        del state.symbols[self.get_symbol()]

class EndDirective(Directive):
    """ Internal class. Implements End Directive. """

    def __init__(self, func):
        super(EndDirective, self).__init__()
        self.eval = func(self)

    def eval(self, state):
         pass

    # YoloSwag
    # The quality of the hack below is suspicious
    # But man, how did that feel good when I saw it working !!!
    @staticmethod
    def endif(self):
        return self._endif

    def _endif(self, state):
        state.decrement_endifs()

    @staticmethod
    def endblock(self):
        return self._endblock

    def _endblock(self, state):
        state.remove_activeblock(self.get_symbol())

class InclDirective(Directive):
    """ Internal class. Implements include directive """
    def eval(self, state):
        fh = None
        guard = InclDirective._create_inclguard(self.get_symbol())
        if guard in state.symbols:
            return
        state.symbols[guard] = None
        try:
            fh = open(self.get_symbol(), "r")
        except:
            state.error("File cannot be opened for reading")
            return 
        state.load_file(fh)

    @staticmethod
    def _create_inclguard(filename):
        return "_" + filename.replace(".", "_").upper() + "_"

class ConditionalDirective(Directive):
    # Apply the same strategy as before for now
    def eval(self, state):
        pass

    def __init__(self, isdef, isif=False):
        super(ConditionalDirective, self).__init__()
        if isif:
            return # Nothing for now
        self.eval = self._ifdef if isdef else self._ifndef

    def _ifdef(self, state):
        if self.get_symbol() in state.symbols:
            state.endifs_awaited += 1
            return
        state.skip_conditional()

    def _ifndef(self, state):
        if not self.get_symbol() in state.symbols:
            state.endifs_awaited += 1
            return
        state.skip_conditional()

    # TODO: Implement more sophisticated conditional
    def _if(self, state):
        pass

    def set_arguments(self, args):
        self.args = args

class BlockDirective(IComparableDirective):
    def __init__(self):
        super(BlockDirective, self).__init__()
        self.first = False # Is it the first line preprocess.

    def get_lines(self):
        return self.block_lines

    def set_lines(self, lines):
        self.block_lines = lines

    def eval(self, state):
        if self.option == BLOCK_OPTIONS["copy"]:
            state.define_symbol(self.get_symbol(), self)
            self.block_lines = list()
            state.do_emit(self)
            state.add_activeblock(self)
            state.increment_endifs()
            return
        if self.get_symbol() not in state.symbols:
            state.error("Block operation cannot be done because of undefined symbol at line %d : %s"
                                        % (state.index, state.current_line()))
            return
        if self.option == BLOCK_OPTIONS["paste"]:
            state.append_blocklines(self.get_symbol())
        elif self.option == BLOCK_OPTIONS["reinject"]:
            state.reinject_blockLines(self.get_symbol())
        elif self.option == BLOCK_OPTIONS["reinterprete"]:
            if not self.get_symbol() in state.symbols or not isinstance(state.symbols[self.get_symbol()], BlockDirective):
                state.error("Invalid symbol for block reinterpretation at line %d: %s" % (state.index, state.current_line()))
                return
            directive = state.symbols[self.get_symbol()]
            lines = directive.get_lines()
            for i in range(len(lines)):
                lines[i] = Replacer.plain_replace(lines[i], state)
            directive.set_lines(lines)

    def append_line(self, orig_line, changed_line, is_normal_line):
        """ 
            Block specific. Appends orig_line if as_is modifier is specified
            Otherwise appends the changed line.
        """
        if self.do_magic:                            # that is, if replacement must be done
            if self.first:
               pass
            elif is_normal_line:                     # if it is not a directive
               self.block_lines.append(changed_line) # accept it in the block
        else:
            if self.first:
                pass
            else:
                self.block_lines.append(orig_line)
        self.first = False

    def set_modifiers(self, modifiers):
        # TODO: rename do_magic member as it means nothing
        self.do_magic = _get_as_is_modifier(modifiers)
        self._emit = _get_emit_modifier(modifiers)
        if not self.do_magic:
            self.first = True

    def set_option(self, option):
        """ Sets block option. Specific to blocks """
        self.option = option

    def should_emit(self):
        """ Blocks specific. Tells whether contents of current block
            should be emitted to output file or stream.
        """
        return self._emit

    def equal(self, other):
        if not isinstance(other, IComparableDirective):
            return False # assert False, "Should not happen"
        if isinstance(other, DefDirective):
            return False # Currently not supported because of optimization in macros with arguments
            if not other_lines and len(self.get_lines()) == 0:
                return True
            other_lines = other_lines.split('\n')
            if len(other_lines) != len(self.block_lines):
                return False
            for i in range(other_lines):
                if other_lines[i] != self.block_lines[i]:
                    return False
            return True
        else:
            other_lines = other.get_lines()
            if len(other_lines) != len(self.block_lines):
                return False
            for i in range(len(other_lines)):
                if other_lines[i] != self.block_lines[i]:
                    return False
            return True

class AssertDirective(Directive):
    def eval(self, state):
        if len(self.operands) == 1:
            isdef = self.operands[0] in state.symbols
            if isdef and self.option == ASSERT_OPTIONS["undef"]:
                self._write_error_quit(state)
            elif not isdef and self.option == ASSERT_OPTIONS["def"]:
                self._write_error_quit(state)
        else:
            op1, op2 = self.operands[0], self.operands[1]
            if not op1 in state.symbols or not op2 in state.symbols:
                state.error("Assertion cannot be done because of undefined symbol on line %d : %s"
                                                                % (state.index, state.current_line))
                return
            dir1, dir2 = state.symbols[op1], state.symbols[op2]
            if (dir1 and not dir2) or (not dir1 and dir2): # they are not both None
                state.error("Assertion cannot be done because of empty operand on line %d : %s"
                                                                % (state.index, state.current_line()))
                return
            if not dir1 and not dir2: # They are both None => equal
                return
            result = dir1.equal(dir2)
            if result and self.option == ASSERT_OPTIONS["not_equal"]:
               self._write_error_quit(state)
            elif not result and self.option == ASSERT_OPTIONS["equal"]:
               self._write_error_quit(state)

    def set_option(self, option):
        self.option = option

    def set_operands(self, operands):
        self.operands = operands

    def _write_error_quit(self, state):
        stderr.write("Assertion failed at line %d: %s. Aborting" % (state.index, state.current_line()))
        exit(2)

class UPreprocessor(object):
    """ Public class. """
    def __init__(self, lines, mode):
        self.state = State(lines)
        self.state.set_parent(self)
        self.replacer = Replacer

    def _warn(self, message):
        self.state.warn(message)

    def _error(self, message):
        self.state.error(message)

    def _create_define(self, i):
        ret = None
        line = self.state.current_line()
        symbol, i = Helper.get_symbol(line, i)
        if symbol == "":
            self.state.warn("Invalid token sequence in define at line %d : %s" % (self.state.index, line))
            return ret
        ret = DefDirective()
        ret.set_symbol(symbol)
        if i < len(line) and line[i] == '(':
            ret.is_invoked = True
            arguments = list()
            while line[i] != ')':
                i += 1;
                arg = ""
                while i < len(line) and line[i].isspace():
                    i += 1
                if i > len(line) or line[i].isalpha() == False:
                    stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.")
                    return None 
                while i < len(line) and line[i].isalpha():
                    arg += line[i];
                    i += 1
                if line[i] != ')' and line[i] != ',' and line[i].isspace == False:
                    stderr.write("Error: Illegal token macro definition encountered. Undefined behaviour.")
                    return None
                arguments.append(arg)
            ret.set_arguments(arguments)
        if line[i] == ')':
            i += 1
        meaning = line[i:].rstrip()
        lines, index = self.state.lines, self.state.index
        if index + 1 < len(lines) and lines[index + 1].find(PREPROC_MULTI) == 0:
            index += 1
            meaning += "\n" + lines[index][len(PREPROC_MULTI):]
            while index + 1 < len(lines) and lines[index + 1].find(PREPROC_MULTI) == 0:
                index += 1
                meaning += lines[index][len(PREPROC_MULTI):]
        self.state.index = index
        ret.set_meaning(meaning)
        return ret
    
    def _create_conditional(self, i, isdef, issimpleif=False):
        ret = None
        line = self.state.current_line()
        symbol, i = Helper.get_symbol(line, i)
        if symbol == "":
            self._warn("Invalid token sequence in conditional on line %d : %s" % (self.state.index, line))
            return None
        if issimpleif:
            # TODO: add a _if method to Directive class
            # Put in symbol everything following the if
            # and evaluate it in the _if method
            funcptr = None
            if symbol == IF_OPTIONS["def"]:
                funcptr = True
            elif symbol == IF_OPTIONS["ndef"]:
                funcptr = False
            else:
                self._warn("Invalid if option on line %d : %s" % (self.state.index, line))
                return None
            ret = ConditionalDirective(isdef=funcptr)
            symbol, i = Helper.get_symbol(line, i)
            if symbol == "":
                self.state.warn("Invalid token sequence in conditional on line %d : %s" % (self.state.index, line))
                return None
        elif isdef:
           ret = ConditionalDirective(isdef=True)
        else:
           ret = ConditionalDirective(isdef=False)
        ret.set_symbol(symbol)
        return ret

    def _create_incl(self, i):
        st = ""
        line = self.state.current_line()
        while i < len(line) and line[i].isspace():
            i += 1
        while i < len(line) and not line[i].isspace():
            st += line[i]
            i += 1
        if st == "":
            self.state.warn("No argument given for include on line %d: %s" % (self.state.index, line))
            return None
        ret = InclDirective()
        ret.set_symbol(st)
        return ret

    def _create_end(self, i):
        line = self.state.current_line()
        option, i = Helper.get_symbol(line, i)
        if option == END_OPTIONS["if"]:
            return EndDirective(EndDirective.endif)
        elif option == END_OPTIONS["block"]:
            symbol, i = Helper.get_symbol(line, i)
            if symbol == "":
                self._error("No symbol for endblock at line %d: %s" % (self.state.index, line))
                return None
            directive = EndDirective(EndDirective.endblock)
            directive.set_symbol(symbol)
            return directive
        self._error("No argument for end directive at line %d : %s" % (self.state.index, line))
        return None

    def _create_block(self, i):
        line = self.state.current_line()
        option, i = Helper.get_symbol(line, i)
        modifiers = create_modifier_list()
        symbol = ""
        if not option in BLOCK_OPTIONS.values():
            self._error("Illegal or no option for block creation at line %d : %s" % (self.state.index, line))
            return None
        while 1:
            modifier, i = Helper.get_symbol(line, i)
            if modifier in MODIFIERS.keys():
                MODIFIERS[modifier](modifiers)
            else:
                symbol = modifier
                break
        if symbol == "":
            self._error("Illegal or no name for block creation at line %d : %s" % (self.state.index, line))
            return None
        directive = BlockDirective()
        directive.set_option(option)
        directive.set_symbol(symbol)
        directive.set_modifiers(modifiers)
        return directive

    def _create_undef(self, i):
        line = self.state.current_line()
        symbol, i = Helper.get_symbol(line, i)
        if symbol == "":
            self._error("Unexpected token in undefine directive at line %d : %s"
                                                    % (self.state.index, line))
            return None
        directive = UndefDirective()
        directive.set_symbol(symbol)
        return directive

    def _create_assert(self, i):
        line = self.state.current_line()
        symbol, i = Helper.get_symbol(line, i)
        if symbol == "":
            self._error("Unexpected token in assert directive at line %d : %s"
                                                    % (self.state.index, line))
            return None
        directive = AssertDirective()
        if not symbol in ASSERT_OPTIONS:
            self._error("Unknown option for assert at line %d : %s" % (self.state.index, line))
            return None
        directive.set_option(symbol)
        op1, i = Helper.get_symbol(line, i)
        if op1 == "":
            self._error("Unexpected token in assert directive at line %d : %s"
                                                    % (self.state.index, line))
            return None
        if symbol == ASSERT_DEFINED or symbol == ASSERT_UNDEFINED:
            directive.set_operands([op1])
        else:
            op2, i = Helper.get_symbol(line, i)
            if op2 == "":
                        self._error("Unexpected token in assert directive at line %d : %s"
                                                                % (self.state.index, line))
                        return None
            directive.set_operands([op1, op2])
        return directive

    def _get_directive(self):
        i = 2
        lines, index = self.state.lines, self.state.index
        lenline = len(lines[index])
        while i < lenline and lines[index][i].isspace():
            i += 1
        if i >= lenline:
            return None
        dirstr = ""
        while i < lenline and lines[index][i].isalpha():
            dirstr += lines[index][i]
            i += 1
        if  dirstr == END:
            return self._create_end(i)
        directive = None
        if dirstr == DEFINE:
            directive = self._create_define(i)
        elif dirstr == UNDEFINE:
            directive = self._create_undef(i)
        elif dirstr == INCL:
            if lines[index][i].isspace() == False:
                self.state.warn("Macro ignored. Invalid token in macro at line %d : %s\n" % (index, lines[index]))
                return None 
            directive = self._create_incl(i)
        elif dirstr == IF:
            directive = self._create_conditional(i, True, True)
        elif dirstr == IFDEF:
            directive = self._create_conditional(i, True)
        elif dirstr == IFNDEF:
            directive = self._create_conditional(i, False)
        elif dirstr == BLOCK:
            directive = self._create_block(i)
        elif dirstr == ASSERT:
            directive = self._create_assert(i)
        if directive and directive.get_meaning() != None:
            directive.set_meaning(self.replacer.plain_replace(directive.get_meaning(), self.state))
        return directive

    def _put_errors(self):
        if self.state.warnings:
            for warning in self.state.warnings:
                stderr.write(warning)
                stderr.write("\n")
        if self.state.errors:
            for error in self.state.errors:
                stderr.write(error)
                stderr.write("\n")
            exit(2)

    def preprocess_lines(self):
        replacer = self.replacer
        while self.state.can_iter():
            line = self.state.current_line()
            if len(line) >= 2 and line[0:2] == PREPROC_PREFIX:
                directive = self._get_directive()
                if directive:
                    directive.eval(self.state)
                self.state.append_readyline(line, replacer, not directive)
            else:
                self.state.append_readyline(line, replacer, True)
            self.state.load_nextline()
        if self.state.endifs_awaited != 0:
            self._error("Unbalanced conditional and end directives - endifs awaited %d" % self.state.endifs_awaited)
        self._put_errors()
        return self.state.get_readylines()

if __name__ == "__main__":
    fh = open(argv[1])
    preproc = UPreprocessor(fh, None)
    fh.close()
    pl = preproc.preprocess_lines()
    for l in pl:
        stdout.write(l)
